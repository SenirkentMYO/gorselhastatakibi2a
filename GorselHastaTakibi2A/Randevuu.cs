﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselHastaTakibi2A
{
    public partial class Randevuu : Form
    {
        public Randevuu()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (HastaneEntities hstn = new HastaneEntities())
            {
                Randevular rnd = new Randevular();
                rnd.HastaAdi = txtAD.Text;
                rnd.HastaSoyAdi = txtSoyAd.Text;
                rnd.HastaTcNO = txtTCNO.Text;
                rnd.PoliklinikAdi = comboBox1.SelectedItem.ToString();
                rnd.RandevuTarihi = Convert.ToDateTime(txtRandevuTarih.Text);

                hstn.Randevular.Add(rnd);
                hstn.SaveChanges();

                MessageBox.Show("KAYIT TAMAMLANDI");

            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (HastaneEntities he = new HastaneEntities())
            {
                var listele = from rnd in he.Randevular
                              where rnd.HastaTcNO == txtARaa.Text
                              select new
                              {
                                  rnd.HastaAdi,
                                  rnd.HastaSoyAdi,
                                  rnd.HastaTcNO,
                                  rnd.PoliklinikAdi,
                                  rnd.RandevuTarihi
                              };
                dataGridView1.DataSource = listele.ToList();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselHastaTakibi2A
{
    public partial class KullanıcıGiris : Form
    {
        public KullanıcıGiris()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Kullanicilar> Liste = new List<Kullanicilar>();
            HastaneEntities he = new HastaneEntities();
            {
                Liste = he.Kullanicilar.Where(a => a.KullaniciAdi == textBox1.Text && a.KullaniciSifre == textBox2.Text).ToList();
                if (Liste.Count > 0)
                {
                    Form1 frm = new Form1();
                    frm.Show();
                }
                else
                {
                    MessageBox.Show("BİLGİLER HATALI");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselHastaTakibi2A
{
    public partial class Poliklinikllerr : Form
    {
        public Poliklinikllerr()
        {
            InitializeComponent();
        }

        private void Poliklinikllerr_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (HastaneEntities hst = new HastaneEntities())
            {
                Poliklinikler pk = new Poliklinikler();

                pk.HastaAdi = txtAd.Text;
                pk.HastalıkTürü = txtHastalkTürü.Text;
                pk.HastaSoyAdi = txtSoyad.Text;
                pk.HastaTcNo = txtTcNo.Text;
                pk.KayıtTarihi = Convert.ToDateTime(txtKayıtTarih.Text);
                pk.PoliklinikAdi = cmbPoliklikAdı.SelectedItem.ToString();
                hst.Poliklinikler.Add(pk);
                hst.SaveChanges();
                MessageBox.Show("İŞLEM GERÇEKLEŞMİŞTİR");


            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (HastaneEntities hst = new HastaneEntities())
            {
                var liste = from plk in hst.Poliklinikler
                            orderby plk.PoliklinikID ascending
                            select new
                            {
                                plk.HastaAdi,
                                plk.HastalıkTürü,
                                plk.HastaSoyAdi,
                                plk.KayıtTarihi,
                                plk.PoliklinikAdi
                            };
                dataGridView1.DataSource = liste.ToList();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
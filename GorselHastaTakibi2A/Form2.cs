﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselHastaTakibi2A
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<HastaBilgileri> hastaliste = new List<HastaBilgileri>();
            HastaneEntities hasta = new HastaneEntities();
            hastaliste = hasta.HastaBilgileri.Where(a => a.Adi.Contains(textBox1.Text)).ToList();
            dataGridView1.DataSource = hastaliste;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselHastaTakibi2A
{
    public partial class Kayıt : Form
    {
        public Kayıt()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (HastaneEntities he = new HastaneEntities())
            {
                HastaBilgileri hb = new HastaBilgileri();
                hb.Adi = txtAdi.Text;
                hb.Adres = txtAdres.Text;
                hb.AnneAdi = txtAnneAdi.Text;
                hb.BabaAdi = txtBabaAdi.Text;
                hb.CepTel = txtCepTel.Text;
                hb.DogumTarihi = Convert.ToDateTime(txtDogumTarihi.Text);
                hb.KanGrubu = cmbKangrub.SelectedText;
                hb.SoyAdi = txtSoyAdi.Text;
                hb.TcNo = txtTcNo.Text;
                he.HastaBilgileri.Add(hb);
                he.SaveChanges();
                MessageBox.Show("KAYIT YAPILMIŞTIR");
            }
        }

        private void btnARA_Click(object sender, EventArgs e)
        {
            using (HastaneEntities he = new HastaneEntities())
            {
                var listele = from h in he.HastaBilgileri
                              where h.TcNo == txtAra.Text
                              select new
                              {
                                  h.Adi,
                                  h.Adres,
                                  h.CepTel,
                                  h.DogumTarihi,
                                  h.KanGrubu,
                                  h.TcNo,
                              };
                dataGridView1.DataSource = listele.ToList();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (HastaneEntities he = new HastaneEntities())
            {
                List<HastaBilgileri> hastaliste = he.HastaBilgileri.ToList();
                HastaBilgileri hb = hastaliste.Last();

                dataGridView1.DataSource = hastaliste.Last();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 frm = new Form1();
            frm.Close();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}